
type objectLearner record { 
     string username;
     string firstName;
     string lastName;
     string [] 'preferred\_formats;
    record {  string course; string score; }  [] 'past\_subjects;
};
type 'Learning\_Material record { 
     string course;
     LearningObject 'learning\_objects;
};
type LearningObject record { 
     MaterialObject [] audio;
     MaterialObject [] video;
     MaterialObject [] text;
};
type MaterialObject record { 
     string name;
     string description;
     string difficulty;
};
type TopicResObject record { 
     string name;
     string description;
     string difficulty;
};
type ReqObject record { 
     string message;
};
type ResObject record { 
     string message;
};