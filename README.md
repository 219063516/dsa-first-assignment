# DSA FIRST ASSIGNMENT

[<img src="https://ballerina.io/img/ballerina-sm-2.jpg " width="600">](http://google.com.au/)
### Member 1
#### Student Name   : Mwetululila Shapaka
#### Student Number : 219063516 

### Member 2
#### Student Name   : David Iilonga
#### Student Number : 219127131 


## PROBLEM 1
OpenAPI creating and updating learner profiles, a particular type of user

```
ballerina new openapi_problem1
cd openapi_problem1
```

OpenAPI definition
```
ballerina openapi gen-contract virtual_learning -i start.bal
```
<img src="defination.PNG" width="600">


Generate the service from OpenAPI contract
```
ballerina openapi gen-service virtual_Profile:virtual_Service virtual_learning.openapi.yaml
```
<img src="contract.PNG" width="700">

Generate the service client from OpenAPI contract.
```
ballerina openapi gen-client virtual_client virtual_learning.openapi.yaml
```
<img src="client_contract.PNG" width="700">


## PROBLEM 2

Build a repository of functions that a developer can assemble to build a complex program

```
ballerina new grpc_problem2

cd grpc_problem2

ballerina add server
ballerina add client

ballerina grpc --input protocol_buffer.proto
ballerina grpc --input protocol_buffer.proto --mode client --output client
ballerina grpc --input protocol_buffer.proto --mode service  --output service 

ballerina run server
ballerina run client
```

Interact with Menu on Client to send requests to Server which will bw listening

<img src="client.PNG" width="300">
