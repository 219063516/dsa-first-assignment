import ballerina/io;
import ballerina/log;
import ballerina/grpc;
import ballerina/lang.'int as ints;

public function user_input() returns new_function {
	string name_ = io:readln("Function name: ");
	string devname_ = io:readln("DevName: ");
	string email = io:readln("Email: ");
	string addr = io:readln("Address: ");
	string lang_ = io:readln("Language: ");
		
	new_function new_func = {func_name: name_,lang: lang_,devName: devname_,email: email,address: addr,version_: 1};
	return <@untained>new_func;
}

public function _add_new_fn(functions_RepositoryBlockingClient ep){	
	var server_res = ep->add_new_fn(user_input());		
	if (server_res is error) {
		log:printError("Connector Error experienced");
	} else {
		string result;
		grpc:Headers resHeaders;
		[result, resHeaders] = server_res;
		io:println(result);
	}
}

service incomming_listener = service {
    resource function onMessage(string rec) {
        io:println(rec);
    }
    resource function onError(error err) {
        io:println("Error from Connector");
    }
    resource function onComplete() {
        io:println("Server completes sending responses");
    }
};

public function _add_fns(){
	functions_RepositoryClient non_ep = new("http://localhost:9091");
	string count = io:readln("Enter number of functions you want to enter: ");
	int|error count_ = ints:fromString(count);	
	new_function[] list_funcs = [];
	
	if (count_ is int) {
		foreach var i in 1...count_ {
			io:println("------------- ENTER FUNCTION "+ i.toString()+" DETAILS ------------------");
			list_funcs.push(user_input());		
		}
		
		repeated_functions multiple_funcs = {
			functions: list_funcs
		};
		
		grpc:Error? response = non_ep->add_fns(multiple_funcs, incomming_listener);
		if (response is grpc:Error){
			log:printError("Error from Connector");
		}else{
			io:println("Connected successfully");
		}
    } else {
        io:println("error input: ", count_.detail());
    }				
}

public function _delete_fn(functions_RepositoryBlockingClient ep){
	string funName = io:readln("Enter Function name: ");
	string ver_ = io:readln("Enter version: ");
	req request_func = {func_name: funName,version_: ver_};
	
	var addResponse = ep->delete_fn(request_func);		
	if (addResponse is error) {
		log:printError("Error from Connector");
	} else {
		string result;
		grpc:Headers resHeaders;
		[result, resHeaders] = addResponse;
		io:println(result);
	}
}

public function _show_fn(functions_RepositoryBlockingClient ep){
	string funName = io:readln("Enter Function name: ");
	string version_ = io:readln("Enter version: ");	
	req request_func = {func_name: funName,version_: version_};
	
	var addResponse = ep->show_fn(request_func);		
	if (addResponse is error) {
		log:printError("Connector Error experienced");
	} else {
		new_function res_function;
		grpc:Headers resHeaders;
		[res_function, resHeaders] = addResponse;
		io:println(res_function);
	}
}

public function _show_all_fns(){
	functions_RepositoryClient non_ep = new("http://localhost:9091");	
	grpc:Error? response = non_ep->show_all_fns("", incomming_listener);
	if (response is grpc:Error){
		log:printError("Error from Connector");
	}else{
		io:println("Connected successfully");
	}
}

public function _show_all_with_criteria(){
	functions_RepositoryClient non_ep = new("http://localhost:9090");
	string lang_ = io:readln("Language: ");	
	
	string count = io:readln("Enter number of Keywords: ");
	int|error cnt = ints:fromString(count);	
	new_function[] list_funcs = [];
	string[] keys_ = [];
	
	if (cnt is int) {
		foreach var i in 1...cnt {
			string key_input = io:readln("Key: ");	
			keys_.push(key_input);
		}
	}
	
	criteria_req criteria = {lang: lang_,keywords: keys_};	
	grpc:Error? response = non_ep->show_all_with_criteria(criteria, incomming_listener);
	if (response is grpc:Error){
		log:printError("Error from Connector");
	}else{
		io:println("Connected successfully");
	}
}

public function main (string... args) {

    functions_RepositoryBlockingClient ep = new("http://localhost:9090");
	
	io:println("=======================");
	io:println("A. ADD NEW FUNCTION:");
	io:println("B. ADD FUNCTIONS:");
	io:println("C. DELETE FUNCTION:");
	io:println("D. SHOW FUNCTION:");
	io:println("E. SHOW ALL FUNCTIONS:");
	io:println("F. SHOW ALL FUNCTIONS WITH CRITERIA:");
    io:println("=======================");
	
    string response = io:readln("INPUT: ");    

    boolean _A = response.equalsIgnoreCaseAscii("A");
    boolean _B = response.equalsIgnoreCaseAscii("B");
    boolean _C = response.equalsIgnoreCaseAscii("C");
	boolean _D = response.equalsIgnoreCaseAscii("D");
	boolean _E = response.equalsIgnoreCaseAscii("C");
	boolean _F = response.equalsIgnoreCaseAscii("F");
	
	if (_A == true) {
		_add_new_fn(ep);
	}else if (_B) {
		_add_fns();
	}else if (_C) {
		_delete_fn(ep);
	}else if (_D) {
		_show_fn(ep);
	}else if (_E) {
		_show_all_fns();
	}else if (_F) {
		_show_all_with_criteria();
	}

}
